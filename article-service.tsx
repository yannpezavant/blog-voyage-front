import axios from "axios";
import { Article } from "entities";

export async function fetchAllArticles(){
    const response = await axios.get<Article[]>('http://localhost:8000/api/article');
    return response.data;
}
export async function fetchOneArticle(id:number){
    const response = await axios.get<Article>('http://localhost:8000/api/article/'+id);
    return response.data;
}
export async function postArticle(article:Article){
    const response = await axios.post<Article>('http://localhost:8000/api/article', article);
    return response.data;
}
export async function updateArticle(article:Article){
    const response = await axios.put<Article>('http://localhost:8000/api/article/'+article.id, article);
    return response.data;
}
export async function deleteArticle(id:any){
    await axios.delete('http://localhost:8000/api/article/'+id);
}