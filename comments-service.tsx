import axios from "axios";
import { Comments } from "entities";

export async function fetchAllComments(){
    const response = await axios.get<Comments[]>('http://localhost:8000/api/comments');
    return response.data;
}
export async function fetchOneComment(id:number){
    const response = await axios.get<Comments>('http://localhost:8000/api/comments/'+id);
    return response.data;
}
export async function postComments(comment:Comments){
    const response = await axios.post<Comments>('http://localhost:8000/api/comments',comment);
    return response.data;
}