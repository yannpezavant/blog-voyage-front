import { Comments } from "entities";
import { useRouter } from "next/router";
import { FormEvent, useState } from "react";
import { postArticle } from "article-service";
import { postComments } from "comments-service";


interface Props{
    onSubmit:(comment:Comments)=>void;
    edited?:Comments;
}


export default function FormComment({onSubmit, edited}:Props){
    const router =useRouter();
    const{id} = router.query;
    const[errors, setErrors]=useState('');

    const [comment, setComment]=useState<Comments>(edited?edited:{
        content:''
    });


        function handleChange(event:any) {
            setComment({
                ...comment,
                [event.target.name]: event.target.value
            });
        }

        // async function handleSubmit(event:FormEvent) {
        //     event.preventDefault();
        //     const ajouter = await postComments(comment);
        //     router.push('/article/'+ajouter.id);
        // }


        async function handleSubmit(event:FormEvent) {
            event.preventDefault();
            
            try{
                onSubmit(comment);
                router.push('/');
            }catch(error:any){
                console.log(error);
                
                if(error.response?.status==400){
                    setErrors(error.response.data.detail);
                }
            }



    return (
        <>
        <form onSubmit={handleSubmit} action="">
                {errors && <p>{errors}</p>}
            <div>
                <label htmlFor="">Add a comment here: </label>
                <input type="text" name="content" value={comment.content} onChange={handleChange} required/>
                <button type="submit">Post</button>
            </div>
        </form>


        </>
    )
}
}
