import { useEffect, useState } from "react";
import { fetchAllComments } from "comments-service";
import { Comments } from "entities";
import ItemComment from "./ItemComment";
import { useRouter } from "next/router";
import { postComments } from "comments-service";
import FormComment from "./FormComment";



export default function Comments(){
    const router = useRouter();
    const [comments, setComments]=useState<Comments[]>([]);

    

    useEffect(()=>{
        fetchAllComments().then(data=>{
          setComments(data);
        });
      }, [])

      async function addComment(commentaire:Comments){
         const ajoutCommentaire = await postComments(commentaire);
        router.push('/article/'+ajoutCommentaire.id);
        console.log(commentaire);
      }


      return(
        <div className="container">
            <div className="row gy-5 gx-5">
                    {comments.map((item) =>
                        <ItemComment key={item.id} comments={item}/>
                    )}
            </div>
            
        </div>



      )

}