import { Comments } from "entities";
import React from "react";


interface Props{
    comments:Comments;
}

export default function ItemComment({comments}:Props){
    return(

        <div className="card" style={{width: 9+"rem"}}>
            <div className="card-body">
                <p className="card-text">{comments.content}</p>
            </div>
        </div>
    )
}