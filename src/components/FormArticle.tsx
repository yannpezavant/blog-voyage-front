import { useState } from "react";
import { Article } from "entities";
import { useRouter } from "next/router";
import { FormEvent } from "react";


interface Props{
    onSubmit:(article:Article)=>void;
    edited?:Article;
}

export default function FormArticle({onSubmit, edited}:Props){
    const router = useRouter();

    const[errors, setErrors]=useState('');

    const [article, setArticle] = useState<Article>(edited?edited:{
        title:'',
        content:'',
        img:'',
        author:'',
        date:''
    });

    function handleChange(event:any) {
        setArticle({
            ...article,
            [event.target.name]: event.target.value
        });
    }

    async function handleSubmit(event:FormEvent) {
        event.preventDefault();
        
        try{
            onSubmit(article);
            router.push('/');
        }catch(error:any){
            console.log(error);
            
            if(error.response?.status==400){
                setErrors(error.response.data.detail);
            }
        }
        // const added = await postArticle(article);
        // router.push('/article/'+ added.id);
    }


    return(

        <>
<div className="formulaire">

        <div className="row form ">
            <form onSubmit={handleSubmit} className="col-md-6">
                {errors && <p>{errors}</p>}
                <div className="mb-3">
                    <label htmlFor="title" className="form-label">Title</label>
                    <input type="text" className="form-control" name="title" value={article.title} onChange={handleChange} required/>
                </div>
                <div className="mb-3">
                    <label htmlFor="content" className="form-label">Content</label>
                    <input type="text" className="form-control" name="content" value={article.content} onChange={handleChange} required/>
                </div>
                <div className="mb-3">
                    <label htmlFor="img" className="form-label">Image URL</label>
                    <input type="text" className="form-control" name="img" value={article.img} onChange={handleChange} />
                </div>
                <div className="mb-3">
                    <label htmlFor="img" className="form-label">Author</label>
                    <input type="text" className="form-control" name="author" value={article.author} onChange={handleChange} required/>
                </div>
                <div className="mb-3">
                    <label htmlFor="date" className="form-label">Date</label>
                    <input type="date" className="form-control" name="date" value={article.date} onChange={handleChange} />
                </div>

                <button type="submit" className="submit btn btn-secondary">Submit</button>
            </form>
        </div>
</div>

        </>
    )
}