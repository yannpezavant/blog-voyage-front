import { Article } from "entities";
import Link from "next/link";
import React from "react";



interface Props{
    article:Article;
}

export default function ItemArticle({article}:Props){
    return(
        <div className="article">
            
                <Link href={"/article/"+article.id} className="btn">
                    <img src={article.img} className="card-img-top" alt="article"/>
                    <div className="card-body">
                    <h5 className="card-title">{article.title}</h5>
                    </div>
                </Link>

            
        </div>
    );
}


