import Link from "next/link";


export default function Header(){

    return(
                <nav className="navbar navbar-expand-lg ">
                    <div className="container-fluid">
                        <img className="minivan" src="https://cdn-icons-png.flaticon.com/128/4120/4120305.png" alt="" />
                        <button className="navbar-toggler bg-light" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon "></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                <li className="nav-item">
                                    <Link className="nav-link" aria-current="page" href={"/"}>Home</Link>
                                </li>
                                <li className="nav-item">
                                    <Link className="nav-link" href={"/article/add"}>Post</Link>
                                </li>
                                <li className="nav-item dropdown">
                                    <a className="nav-link dropdown-toggle " href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        Categories
                                    </a>
                                    <ul className="dropdown-menu">
                                        <li><a className="dropdown-item" href="#">Europe</a></li>
                                        <li><a className="dropdown-item" href="#">America</a></li>
                                        <li><a className="dropdown-item" href="#">Best restaurants</a></li>
                                        <li><a className="dropdown-item" href="#">Best capital cities</a></li>
                                    </ul>
                                </li>
                                
                            </ul>
                            <form className="d-flex" role="search">
                                <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search"/>
                                    <button className="btn btn-outline-light" type="submit">Search</button>
                            </form>
                        </div>
                    </div>
                </nav>

    )
}
