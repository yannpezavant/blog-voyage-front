import { useState } from "react";
import { Article } from "entities";
import { useEffect } from "react";
import { fetchAllArticles } from "article-service";
import ItemArticle from "./ItemArticle";

export default function Articles(){
    const [articles, setArticles]= useState<Article[]>([]);
    
    useEffect(()=>{
        fetchAllArticles().then(data=>{
          setArticles(data);
        });
      }, [])


    return(
        
        
            
        <div className="articles row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4 g-4 justify-content-center">
        {articles.map((item) =>
                        <ItemArticle key={item.id} article={item}/>
                    )}
        </div>
        
        
        
    )
}

