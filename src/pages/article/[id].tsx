import { useRouter } from "next/router";
import { useState } from "react";
import { Article, Comments } from "entities";
import { useEffect } from "react";
import { updateArticle, deleteArticle, fetchOneArticle } from "article-service";
import FormArticle from "@/components/FormArticle";

import Header from "@/components/Header";
import FormComment from "@/components/FormComment";
import { fetchOneComment, postComments } from "comments-service";
import ItemComment from "@/components/ItemComment";





export default function ArticlePage() {

    const router = useRouter();
    const { id } = router.query;
    const [article, setArticle] = useState<Article>();
    const [comment, setComment] = useState<Comments>();
    const [showEdit, setShowEdit] = useState(false);

    useEffect(() => {

        if (!id) {
            return;
        }
        fetchOneArticle(Number(id))
            .then(data => setArticle(data))
            .catch(error => {
                router.push('/404');
            });

    }, [id]);

    async function remove() {
        await deleteArticle(id);
        router.push('/');
    }
    async function update(article: Article) {
        const updated = await updateArticle(article);
        setArticle(updated);
    }
    async function updateComment(commentaire: Comments) {
        const addComment = await postComments(commentaire);
        setComment(addComment);
    }

    async function toggle() {
        setShowEdit(!showEdit);
    }





    if (!article) {
        return <p>Loading ...</p>
    }
    return (
        <>
            <Header />

            <div className="d-flex justify-content-center">
                <div className="card" style={{ width: 30 + "rem" }}>
                    <img src={article.img} className="card-img-top" alt="image" />
                    <div className="card-body">
                        <h5 className="article_title card-title">{article.title}</h5>
                        <p className="card-text">{article.content}</p>
                        <p className="card-text">{article.author}</p>
                        <div className="d-flex">
                            <p className="card-text ">{article.date &&
                                <p>{new Date(article.date).toLocaleDateString()}</p>
                        }</p>
                        </div>
                        <div className="boutons">

                            <button className="boutons-article" onClick={remove}>Delete</button>
                            <button className="boutons-article" onClick={toggle}>Edit</button>
                        </div>

                    </div>
                </div>
            </div>
            

            <div>

            {showEdit &&
                <>
                    <h2 className="edit_article">Edit Article</h2>
                    <div className="edit">
                        <FormArticle edited={article} onSubmit={update} />
                    </div>
                </>
            }
            </div>
        </>

    );
}