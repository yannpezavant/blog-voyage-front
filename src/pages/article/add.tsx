import FormArticle from "@/components/FormArticle";
import Header from "@/components/Header";
import { useRouter } from "next/router";
import { Article } from "entities";
import { postArticle } from "article-service";




export default function addArticle(){
    const router = useRouter();

    async function addArticle(article:Article){
        const added = await postArticle(article); 
        router.push('/article/'+added.id);
    }

    return(
        <>
        <Header/>
        <FormArticle onSubmit={addArticle}/>
        </>
        
    )
}

