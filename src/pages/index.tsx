import Header from "@/components/Header";
import Articles from "@/components/Articles";


import Commentss from "@/components/Comments";
import ListComments from "@/components/Comments";
import Comments from "@/components/Comments";
import ItemComment from "@/components/ItemComment";




export default function Index() {

    return (
        <>
            <Header />
            <div className="card banner text-bg-dark">
                <img src="https://cdn.pixabay.com/photo/2021/08/20/20/05/mountains-6561191__340.jpg" className="card-banner" alt="banner-img"/>
                    <div className="card-img-overlay">
                        <h5 className="card-title">Explore your world</h5>
                    </div>
            </div>
            <h3 className="top_destinations">Top Destinations</h3>
            <Articles />
            <footer className="footer">
                <p>© Wanderlust 2023</p>
            </footer>
            
        </>
    );
}