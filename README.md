Le projet Blog est un projet fullstack. Le thème de ce blog est celui du voyage.

La partie backend à été développée en PHP, MySQL et avec le framework Symfony.

La partie frontend à été développée en HTML, CSS et Typescript et avec le framework React/Next.Js.

Fonctionnalités:

-voir la liste des articles

-voir la page d'un article

-éditer un article

-supprimer un article

-poster un article


Dans le projet, le script back contient les entités, composants et controlleurs des classes counter, comments et category mais ne sont pas utilisé dans le front.

Wireframe: https://www.figma.com/file/JA2woT6cMhByH931IbkVhj/Blog?node-id=0%3A1&t=ZF4pgwEAO1phs3Hq-0


